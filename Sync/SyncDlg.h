﻿
// SyncDlg.h: файл заголовка
//

#pragma once
#define _CRT_SECURE_NO_WARNINGS


// Диалоговое окно CSyncDlg
class CSyncDlg : public CDialogEx
{
// Создание
public:
	CSyncDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SYNC_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;
	HANDLE hThread;
	BOOL triger;
	HANDLE event1;
	HANDLE event2;
	HANDLE event3;
	HANDLE hMap;
	LPVOID p_memory;
	friend DWORD WINAPI thread(PVOID t);

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();
	CString str_box;
	CEdit control_box;

	afx_msg void OnEnChangeText();
};
