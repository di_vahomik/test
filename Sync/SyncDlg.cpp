﻿
// SyncDlg.cpp: файл реализации
//
#define _CRT_SECURE_NO_WARNINGS

#include "pch.h"
#include "framework.h"
#include "Sync.h"
#include "SyncDlg.h"
#include "afxdialogex.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CSyncDlg



CSyncDlg::CSyncDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SYNC_DIALOG, pParent)
	, str_box(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSyncDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TEXT, str_box);
	DDX_Control(pDX, IDC_TEXT, control_box);
}

BEGIN_MESSAGE_MAP(CSyncDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CSyncDlg::OnBnClickedCancel)
	ON_EN_CHANGE(IDC_TEXT, &CSyncDlg::OnEnChangeText)
END_MESSAGE_MAP()


// Обработчики сообщений CSyncDlg

BOOL CSyncDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок
	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_NORMAL);
	event1 = CreateEvent(NULL, TRUE, FALSE, L"FirstStep");
	event2 = CreateEvent(NULL, TRUE, FALSE, L"SecondStep");
	event3 = CreateEvent(NULL, FALSE, FALSE, L"box");
	hMap = CreateFileMapping(NULL, NULL, PAGE_READWRITE, 0, 100, L"TEXT");
	p_memory = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, NULL);
	if (event1 != NULL) {
		DWORD dw = WaitForSingleObject(event1, 0);
		switch (dw) {
		case WAIT_OBJECT_0:
			control_box.SetReadOnly(TRUE);
			hThread = CreateThread(NULL, 0, thread, this, NULL, NULL);
			break;
		case WAIT_TIMEOUT:
			
			SetEvent(event1);
			break;
		}
	}

	// TODO: добавьте дополнительную инициализацию

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.


DWORD WINAPI thread(PVOID t) {

	CSyncDlg* cs = (CSyncDlg*)t;
	HANDLE event_array[2] = { cs->event2, cs->event3 };
	while (1) {
		DWORD dw1 = WaitForMultipleObjects(2, event_array, FALSE, INFINITE);
		switch (dw1 - WAIT_OBJECT_0) {
		case 0:
		{
			cs->control_box.SetWindowText((wchar_t*)cs->p_memory);
			ResetEvent(cs->event2);
			break;
		}
		case 1:
			cs->control_box.SetReadOnly(FALSE);
			return 0;
		}


	}
	return 0;
}

void CSyncDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CSyncDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CSyncDlg::OnBnClickedCancel()
{
	// TODO: добавьте свой код обработчика уведомлений
	SetEvent(event3);
	CDialogEx::OnCancel();
}


void CSyncDlg::OnEnChangeText()
{
	SetEvent(event2);
	wchar_t* wch_source;
	UpdateData(TRUE);
	wcscpy((wchar_t*)p_memory, str_box.GetBuffer());
}
